var express = require('express');
var dotenv = require('dotenv').config();
var parser = require('body-parser');
var cors = require('cors');
var routes = require('./src/routes');
var app = express();

app.use(parser.json());
app.use(parser.urlencoded({extended: false}));
app.use(cors({exposedHeaders: ['Location']}));

app.use('', routes);

var port = process.env.PORT || 3000;
var server = app.listen(port, function () {
  console.log('Server is listening on port ' + port);
});
