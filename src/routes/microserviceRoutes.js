let router = require('express').Router();
let microserviceCtrl = require('../controllers/microserviceCtrl');
let authorize = require('../middlewares/authorize');
let applyRichardsonLinks = require('../middlewares/applyRichardsonLinks');

router.route('/')
  .get(microserviceCtrl.getAll, applyRichardsonLinks)
  .post(authorize, microserviceCtrl.create);

router.route('/:microserviceId')
  .get(microserviceCtrl.getById)
  .put(authorize, microserviceCtrl.update)
  .delete(authorize, microserviceCtrl.delete);

module.exports = router;
