let router = require('express').Router();
let categoryCtrl = require('../controllers/categoryCtrl');
let authorize = require('../middlewares/authorize');
let applyRichardsonLinks = require('../middlewares/applyRichardsonLinks');

router.route('/')
  .get(categoryCtrl.getAll, applyRichardsonLinks)
  .post(authorize, categoryCtrl.create);

router.route('/:categoryId')
  .get(categoryCtrl.getById)
  .put(authorize, categoryCtrl.update)
  .delete(authorize, categoryCtrl.delete);

module.exports = router;
