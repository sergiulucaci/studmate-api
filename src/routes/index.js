var router = require('express').Router();
var microserviceRoutes = require('./microserviceRoutes');
var categoryRoutes = require('./categoryRoutes');

router.use('/microservices', microserviceRoutes);
router.use('/categories', categoryRoutes);

module.exports = router;