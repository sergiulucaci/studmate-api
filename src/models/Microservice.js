var thinky = require('../lib/thinky');
var r = thinky.r;
var type = thinky.type;

const Microservice = thinky.createModel("Microservice", {
  id: type.string(),
  title: type.string().required(),
  description: type.string().required(),
  source: type.string().required(),
  price: type.number().required(),
  createdAt: type.date().default(r.now()),
  updatedAt: type.date().default(r.now()),
  deleted: type.boolean().default(false)
});

module.exports = Microservice;