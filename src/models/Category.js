var thinky = require('../lib/thinky');
var r = thinky.r;
var type = thinky.type;

const Category = thinky.createModel("Category", {
  id: type.string(),
  name: type.string().required(),
  createdAt: type.date().default(r.now()),
  updatedAt: type.date().default(r.now()),
  deleted: type.boolean().default(false)
});

module.exports = Category;