var token = require('./../utils/token');

module.exports = function (req, res, next) {
  var apiToken = req.body.token || req.query.token || req.headers['x-access-token'] || (req.headers['authorization'] && req.headers['authorization'].replace('Bearer ', ''));
  let decodedToken = token.verify(apiToken, next);
  req.auth = decodedToken;
  next();
};
