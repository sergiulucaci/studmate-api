let categoryRepo = require('../repositories/categoryRepo');
let handleError = require('../utils/handleError');
let verifyPagination = require('../utils/verifyPagination');

/*
 Get all categories
 Returns all categories as an array of JSON objects. Limited by default to 15 results.
 */
module.exports.getAll = function (req, res, next) {
  categoryRepo.count()
    .then(function (count) {
      if (count > 0) {
        // Verify if the page and limit provided through req.query strings are within range
        let pagination = verifyPagination(req.query.limit, req.query.page, count);
        let limit = pagination.limit;
        let page = pagination.page;

        categoryRepo.getAllFiltered(req.query)
          .then(function (allData) {
            // Fetch all the data now, with the page and limit assured to be in range
            categoryRepo.getAll(limit, page, req.query)
              .then(function (categories) {
                req.data = categories;
                req.meta = {
                  page: page,
                  limit: limit,
                  count: count,
                  totalItems: allData.length,
                  endpoint: 'categories'
                };
                next();
              })
              .catch(handleError(res))
          }).catch(handleError(res))
      }
      else {
        res.json({success: false, message: "No results found"})
      }
    }).catch(handleError(res))
};

/*
 Get a single category
 Returns a single Category object. Request by categoryId.
 */
module.exports.getById = function (req, res) {
  categoryRepo.getById(req.params.categoryId)
    .then(function (category) {
      res.json(category);
    })
    .catch(handleError(res))
};

/*
 Create a category
 Creates a category resource. Returns Category object.
 */
module.exports.create = function (req, res) {
  categoryRepo.create(req.body)
    .then(function (category) {
      res.json(category)
    })
    .catch(handleError(res));
};

/*
 Update a category
 Update uses the PUT method. PUT implies putting a resource, completely replacing whatever is available at the given URL.
 Returns a Category object
 */
module.exports.update = function (req, res) {
  req.body.updatedAt = new Date();
  categoryRepo.getById(req.params.categoryId)
    .then(function (category) {
      if (category.userId !== req.auth.id) {
        res.json({success: false, errorMessage: 'This category doesn\'t belongs to the current user.'});
      } else {
        categoryRepo.update(req.params.categoryId, req.body)
          .then(function (categoryId) {
            res.json(categoryId);
          })
          .catch(handleError(res))
      }
    })
    .catch(handleError(res));
};

/*
 Soft Delete a Category.
 Returns a Category object
 */
module.exports.delete = function (req, res) {
  categoryRepo.getById(req.params.categoryId)
    .then(function (category) {
      if (category.userId !== req.auth.id) {
        res.json({success: false, errorMessage: 'This category doesn\'t belongs to the current user.'});
      } else {
        categoryRepo.delete(req.params.categoryId)
          .then(function (categoryId) {
            res.json(categoryId)
          })
          .catch(handleError(res))
      }
    })
    .catch(handleError(res))
};