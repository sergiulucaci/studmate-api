let microserviceRepo = require('../repositories/microserviceRepo');
let handleError = require('../utils/handleError');
let verifyPagination = require('../utils/verifyPagination');

/*
 Get all microservices
 Returns all microservices as an array of JSON objects. Limited by default to 15 results.
 */
module.exports.getAll = function (req, res, next) {
  microserviceRepo.count()
    .then(function (count) {
      if (count > 0) {
        // Verify if the page and limit provided through req.query strings are within range
        let pagination = verifyPagination(req.query.limit, req.query.page, count);
        let limit = pagination.limit;
        let page = pagination.page;

        microserviceRepo.getAllFiltered(req.query)
          .then(function (allData) {
            // Fetch all the data now, with the page and limit assured to be in range
            microserviceRepo.getAll(limit, page, req.query)
              .then(function (microservices) {
                req.data = microservices;
                req.meta = {
                  page: page,
                  limit: limit,
                  count: count,
                  totalItems: allData.length,
                  endpoint: 'microservices'
                };
                next();
              })
              .catch(handleError(res))
          }).catch(handleError(res))
      }
      else {
        res.json({success: false, message: "No results found"})
      }
    }).catch(handleError(res))
};

/*
 Get a single microservice
 Returns a single Microservice object. Request by microserviceId.
 */
module.exports.getById = function (req, res) {
  microserviceRepo.getById(req.params.microserviceId)
    .then(function (microservice) {
      res.json(microservice);
    })
    .catch(handleError(res))
};

/*
 Create a microservice
 Creates a microservice resource. Returns Microservice object.
 */
module.exports.create = function (req, res) {
  microserviceRepo.create(req.body)
    .then(function (microservice) {
      res.json(microservice)
    })
    .catch(handleError(res));
};

/*
 Update a microservice
 Update uses the PUT method. PUT implies putting a resource, completely replacing whatever is available at the given URL.
 Returns a Microservice object
 */
module.exports.update = function (req, res) {
  req.body.updatedAt = new Date();
  microserviceRepo.getById(req.params.microserviceId)
    .then(function (microservice) {
      if (microservice.userId !== req.auth.id) {
        res.json({success: false, errorMessage: 'This microservices doesn\'t belongs to the current user.'});
      } else {
        microserviceRepo.update(req.params.microserviceId, req.body)
          .then(function (microserviceId) {
            res.json(microserviceId);
          })
          .catch(handleError(res))
      }
    })
    .catch(handleError(res));
};

/*
 Soft Delete a Microservice.
 Returns a Microservice object
 */
module.exports.delete = function (req, res) {
  microserviceRepo.getById(req.params.microserviceId)
    .then(function (microservice) {
      if (microservice.userId !== req.auth.id) {
        res.json({success: false, errorMessage: 'This microservices doesn\'t belongs to the current user.'});
      } else {
        microserviceRepo.delete(req.params.microserviceId)
          .then(function (microserviceId) {
            res.json(microserviceId)
          })
          .catch(handleError(res))
      }
    })
    .catch(handleError(res))
};