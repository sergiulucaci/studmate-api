var thinky = require('../lib/thinky');
var r = thinky.r;
var Category = require('../models/Category');

/*
 Returns all Categories as an array of JSON objects. Limit is set to 15 by default
 */
module.exports.getAll = function (limit, page, q) {
  return Category
    .filter(function (row) {
      var res = row('deleted').eq(false);
      if (q.title) res = res.and(row('title').match("(?i)" + q.title));
      return res;
    })
    .skip(limit && page ? (page - 1) * limit : 0)
    .limit(limit ? limit : 0)
    .orderBy(r.asc(function (row) {
      if (!q.sort) {
        return row('title');
      }
      return row(q.sort)
    }))
    .run();
};

module.exports.getAllFiltered = function (q) {
  return Category
    .filter(function (row) {
      var res = row('deleted').eq(false);
      if (q.title) res = res.and(row('title').match("(?i)" + q.title));
      return res;
    })
    .run();
};

/*
 Returns a single Category object. Request by categoryId
 */
module.exports.getById = function (id) {
  return Category
    .get(id)
    .run();
};

/*
 Create a Category
 Creates a Category resource. Returns a Category object
 */
module.exports.create = function (data) {
  return Category
    .save(data)
    .then(function (result) {
      return result;
    }).error(function (err) {
      return err;
    })
};

/*
 Update a Category
 Update uses the PUT method. PUT implies putting a resource, completely replacing whatever is available at the given URL
 Returns a Category object
 */
module.exports.update = function (id, data) {
  return Category
    .get(id)
    .update(data)
    .run()
    .then(function (result) {
      return result;
    }).error(function (err) {
      return err;
    })
};

/*
 Delete a category resource. Request by categoryId
 Returns a Category object
 */
module.exports.delete = function (id) {
  return Category
    .get(id)
    .update({deleted: true})
    .run()
    .then(function (result) {
      return result;
    }).error(function (err) {
      return err;
    })
};

/*
 Counts the number of entries in the Categories table
 */
module.exports.count = function () {
  return Category
    .count()
    .execute()
};
