var thinky = require('../lib/thinky');
var r = thinky.r;
var Microservice = require('../models/Microservice');

/*
 Returns all Microservices as an array of JSON objects. Limit is set to 15 by default
 */
module.exports.getAll = function (limit, page, q) {
  return Microservice
    .filter(function (row) {
      var res = row('deleted').eq(false);
      if (q.title) res = res.and(row('title').match("(?i)" + q.title));
      return res;
    })
    .skip(limit && page ? (page - 1) * limit : 0)
    .limit(limit ? limit : 0)
    .orderBy(r.asc(function (row) {
      if (!q.sort) {
        return row('title');
      }
      return row(q.sort)
    }))
    .run();
};

module.exports.getAllFiltered = function (q) {
  return Microservice
    .filter(function (row) {
      var res = row('deleted').eq(false);
      if (q.title) res = res.and(row('title').match("(?i)" + q.title));
      return res;
    })
    .run();
};

/*
 Returns a single Microservice object. Request by microserviceId
 */
module.exports.getById = function (id) {
  return Microservice
    .get(id)
    .run();
};

/*
 Create a Microservice
 Creates a Microservice resource. Returns a Microservice object
 */
module.exports.create = function (data) {
  return Microservice
    .save(data)
    .then(function (result) {
      return result;
    }).error(function (err) {
      return err;
    })
};

/*
 Update a Microservice
 Update uses the PUT method. PUT implies putting a resource, completely replacing whatever is available at the given URL
 Returns a Microservice object
 */
module.exports.update = function (id, data) {
  return Microservice
    .get(id)
    .update(data)
    .run()
    .then(function (result) {
      return result;
    }).error(function (err) {
      return err;
    })
};

/*
 Delete a microservice resource. Request by microserviceId
 Returns a Microservice object
 */
module.exports.delete = function (id) {
  return Microservice
    .get(id)
    .update({deleted: true})
    .run()
    .then(function (result) {
      return result;
    }).error(function (err) {
      return err;
    })
};

/*
 Counts the number of entries in the Microservices table
 */
module.exports.count = function () {
  return Microservice
    .count()
    .execute()
};
