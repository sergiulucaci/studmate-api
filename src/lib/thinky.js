// For development purposes only
var config = require('../config');
var thinky = require('thinky')(config.database);

module.exports = thinky;