var rdb = require('rethinkdb');
var async = require('async');

var db = process.env.DB_NAME;
var host = process.env.DB_HOST;
var port = process.env.DB_PORT;

var dropDb = function (next) {
  rdb.connect({host: host, port: port}, function (error, connection) {
    rdb.dbDrop(db).run(connection, function (error, response) {
      connection.close();
      next(error, response);
    });
  });
};

var createDb = function (next) {
  rdb.connect({host: host, port: port}, function (error, connection) {
    rdb.dbCreate(db).run(connection, function (error, response) {
      connection.close();
      next(error, response);
    });
  });
};


async.series({
  dropped: dropDb,
  created: createDb
}, function (error, response) {
  console.log(response);
});