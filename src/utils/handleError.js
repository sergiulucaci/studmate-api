module.exports = function handleError(res) {
  return function (err) {
    res.status(404).json({success: false, message: err.message})
  }
};