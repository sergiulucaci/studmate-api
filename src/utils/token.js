var jwt = require('jwt-simple');
var moment = require('moment');
var config = require('../config');

module.exports.generate = function (data) {
  // Set expiration date for the token
  var expires = moment().add(7, 'days').valueOf();

  return jwt.encode({iss: data, exp: expires}, config.app.token);
};

module.exports.verify = function (token, next) {
  if (!token) {
    var notFoundError = new Error('Token not found');
    notFoundError.status = 404;

    return next(notFoundError);
  }

  if (jwt.decode(token, config.app.token) <= moment().format('x')) {
    var expiredError = new Error('Token has expired');
    expiredError.status = 401;

    return next(expiredError);
  }

  return jwt.decode(token, config.app.token);
};