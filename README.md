Step 1. Install RethinkDB on OSX:
```
brew update && brew install rethinkdb
```

Step 2. Start RethinkDB server (default port 8080):
```
rethinkdb
```

Step 3. Open another terminal tab. Install dependencies:
```
npm install
```

Step 4. Start node server (default port is 8000, and if is requested permission type >> *sudo nodemon server.js* instead of the below command):
```
nodemon server.js
```